class AddIsadministratorToUsers < ActiveRecord::Migration
  def change
    add_column :users, :isadministrator, :boolean
  end
end
