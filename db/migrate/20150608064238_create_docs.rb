class CreateDocs < ActiveRecord::Migration
  def change
    create_table :docs do |t|
      t.string :author
      t.string :description
      t.string :title
      t.references :categories, index: true
      t.string :filename
      t.string :path

      t.timestamps null: false
    end
    add_foreign_key :docs, :categories
  end
end
