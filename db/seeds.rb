# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)
User.create(name: "Admin", email: "admin@admin.com", password: "admin123", password_confirmation: "admin123", isadministrator: true, cpf: "684.436.275-73")
User.create(name: "User", email: "demo@demo.com", password: "demo123", password_confirmation: "demo123", isadministrator: false, cpf: "963.520.802-27")
Category.create(name: "Bills")
Category.create(name: "Student")