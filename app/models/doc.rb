class Doc < ActiveRecord::Base
  belongs_to :categories
  validates :title , presence: true, uniqueness: true
  validates :author , presence: true
  validates :description , presence: true
  validates :categories_id , presence: true
  
   # It returns the docs whose titles contain one or more words that form the query
  def self.search(query)
    # where(:title, query) -> This would return an exact match of the query
    where("title like ?", "%#{query}%") 
  end
end
