class DocsController < ApplicationController
  before_action :ensure_logged
  def index
    if params[:search]
      @files = Doc.search(params[:search]).order("created_at DESC")
    else
      @files = Doc.order("created_at DESC")
    end
  end

  def new
    @file = Doc.new(params[:doc_params])
  end
  
  def save
    uploaded_io = params[:doc][:filename]
    @path = File.open(Rails.root.join('public', 'uploads', uploaded_io.original_filename), 'wb') do |file|
    file.write(uploaded_io.read)
    end
    file = params[:doc]
    newfile = Doc.new()
    newfile.title = file[:title]
    newfile.author = file[:author]
    newfile.categories_id = file[:categories_id]
    newfile.description = file[:description]
    newfile.path = File.join(Rails.root, "public/uploads",uploaded_io.original_filename)
    newfile.filename = uploaded_io.original_filename
    newfile.save
    redirect_to docs_path
  end
  
  private

  def doc_params
    params.require(:doc).permit(:author, :description, :title, :categories_id, :filename, :path)
  end
end
